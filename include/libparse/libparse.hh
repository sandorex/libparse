// Copyright 2018-2019 Aleksandar Radivojevic
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// You may obtain a copy of the License at
// 	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef libparse_VERSION_MAJOR
#include <algorithm>	// std::any_of, std::mismatch, std::any_of
#include <cctype>		  // isspace, isalpha..
#include <deque>		  // std::deque
#include <string>		  // std::string_view
#include <type_traits> // std::conditional_t, std::is_same_v
#include <utility>	  // std::initializer_list

#define libparse_VERSION_MAJOR 0
#define libparse_VERSION_MINOR 1
#define libparse_VERSION_PATCH 0
#define libparse_VERSION_STRING "0.1.0"
#endif

#ifndef libparse_NAMESPACE
#define libparse_NAMESPACE libparse
#endif

// TODO: real benchmarks on bigger files
// TODO: tests for conditionals
// TODO: tests for <cctype> stuff
// TODO: debugging
// TODO: rework Capture to use data argument instead of static variables
namespace libparse_NAMESPACE {
#ifdef libparse_CONTAINER
	using ContainerType = libparse_TYPE;
#else
	using ContainerType = std::string_view;
#endif

	using Type = ContainerType::value_type;

#ifdef libparse_ITERATOR
	using Iterator = libparse_ITERATOR;
	using ConstIterator = const libparse_ITERATOR;
#else
	using Iterator = ContainerType::const_iterator;
	using ConstIterator = const ContainerType::const_iterator;
#endif

	// appropriate container for storing Type captures
#ifdef libparse_CAPTURE_CONTAINER
	using CaptureContainerType = libparse_CAPTURE_CONTAINER;
#else
	// std::string_view cannot be constructed with two iterators
	using CaptureContainerType = std::conditional_t<
		std::is_same_v<ContainerType, std::string_view> || std::is_same_v<ContainerType, std::string>,
		std::string,
		std::deque<Type>>;
#endif

	namespace internal {
		// when there is a flag conflict, higher value flag takes precedence
		enum Flags {
			// does a rule have an action
			// bool action(bool, D&, ConstIterator&, ConstIterator&)
			// bool argument is whether or not matcher function
			// (true if neither Data nor Matcher flag are passed)
			// D is any type provided to match function
			Action = 1,

			// does rule have matcher data
			// static constexpr CONTAINER<Type> data = { ... };
			// CONTAINER must have begin(), end() accessible
			//
			// cannot be used with Matcher flag
			Data = 1 << 1,

			// does rule have a matcher function
			// bool process(Iterator&, ConstIterator&, D&)
			// D is any type provided to match function
			//
			// cannot be used with Data flag
			Matcher = 1 << 2,

			// check if iterator is out of bounds
			// before running matcher function
			//
			// ignored if Matcher flag has not been passed
			MatcherCheckIteratorBounds = 1 << 3,

			// saves iterator before running matcher function
			// and advances the iterator only if
			// matcher function returns true
			//
			// ignored if Matcher flag has not been passed
			MatcherAdvanceIteratorOnMatch = 1 << 4,

			// checks if any of data matches
			// checks all of it by default
			//
			// ignored if Data flag has not been passed
			DataAnyOf = 1 << 5
		};

		// checks if a rule could advance the iterator or not
		template <class T>
		constexpr bool advances_iterator() {
			return T::flags & (Flags::Matcher | Flags::Data);
		}

		// used for reporting errors using static_assert
		template <typename...>
		constexpr bool false_v = false;

		// processes matcher rules
		template <class T, typename D>
		bool process_matcher(Iterator& iterator, ConstIterator& end, D& data) {
			if constexpr (T::flags & Flags::MatcherCheckIteratorBounds)
				if (iterator >= end)
					return false;

			if constexpr (T::flags & Flags::MatcherAdvanceIteratorOnMatch) {
				if (auto iter = iterator; T::process(iter, end, data)) {
					iterator = iter;
					return true;
				}

				return false;
			} else
				return T::process(iterator, end, data);
		}

		// processes data rules
		template <class T>
		bool process_data(Iterator& iterator, ConstIterator& end) {
			if constexpr (T::data.size() == 1) {
				if (iterator < end && *iterator == *T::data.begin()) {
					++iterator;
					return true;
				}
			} else if constexpr (T::flags & Flags::DataAnyOf) {
				if (iterator >= end)
					return false;

				const auto pred = [&iterator](auto& a) {
					return *iterator == a;
				};

				if (std::any_of(T::data.begin(), T::data.end(), pred)) {
					++iterator;

					return true;
				}
			} else {
				if (iterator + T::data.size() > end)
					return false;

				if (auto result = std::mismatch(T::data.begin(), T::data.end(), iterator); result.first == T::data.end()) {
					iterator = result.second;

					return true;
				}
			}

			return false;
		}

		// splits types of rules
		template <class T, typename D>
		bool process(Iterator& iterator, ConstIterator& end, D& data) {
			bool result;

			if constexpr (T::flags & Flags::Matcher)
				result = process_matcher<T>(iterator, end, data);
			else if constexpr (T::flags & Flags::Data)
				result = process_data<T>(iterator, end);
			else if constexpr (T::flags & Flags::Action)
				result = true;
			else
				static_assert(false_v<T>, "Rule must be have either Matcher, Data or Action flag to be a valid rule");

			if constexpr (T::flags & Flags::Action)
				T::action(result, data, static_cast<const Iterator>(iterator), end);

			return result;
		};

		template <class T, int Flags>
		struct Rule {
			static_assert(Flags & (Flags::Matcher | Flags::Data | Flags::Action), "Rule must be have either Matcher, Data or Action flag to be a valid rule");

			static constexpr auto flags = Flags;

			// gets full name of rule with its template arguments
			static constexpr std::string_view get_full_info() {
#ifdef _WIN32
				std::string_view str(__FUNCSIG__);
				str.remove_prefix(str.find("Rule"));
				str.remove_suffix(16);
#else
				std::string_view str(__PRETTY_FUNCTION__);
				str.remove_prefix(str.find("["));
#endif
				return str;
			}

			// gets name of the rule
			static constexpr std::string_view get_name() {
#ifdef _WIN32
				//return typeid(Rule).__name;
				auto str = get_full_info();
				const std::string_view find_str = "class ";
				str.remove_prefix(str.find(find_str) + find_str.size());
				str = str.substr(0, str.find("<"));

				return str;
#else
				auto str = get_full_info();
				const std::string_view find_str = "with T = ";
				str.remove_prefix(str.find(find_str) + find_str.size());
				str = str.substr(0, str.find("<"));

				return str;
#endif
			}

			// basically an alias to match with this rule
			template <typename D>
			static bool match(Iterator& iterator, ConstIterator& end, D& data) {
				return process<T>(iterator, end, data);
			}

			static bool match(Iterator& iterator, ConstIterator& end) {
				// dummy is added to disable data argument
				bool dummy = false;
				return process<T>(iterator, end, dummy);
			}

			template <typename D>
			static bool match(ConstIterator& iterator, ConstIterator& end, D& data) {
				auto iter = iterator;
				return match(iter, end, data);
			}

			static bool match(ConstIterator& iterator, ConstIterator& end) {
				auto iter = iterator;
				return match(iter, end);
			}
		};
	}

	// special
	template <class... T>
	struct Capture : internal::Rule<
							  Capture<T...>,
							  (sizeof...(T) == 0 ? internal::Flags::Action
														: internal::Flags::Matcher)
								  | internal::Flags::MatcherCheckIteratorBounds> {
		static inline std::deque<CaptureContainerType> captures;

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			if (auto iter = iterator; (... && internal::process<T>(iter, end, data))) {
				captures.emplace_back(iterator, iter);
				iterator = iter;
				return true;
			}

			return false;
		}

		template <typename D>
		static void action(bool, D& data, ConstIterator& iterator, ConstIterator& end) {
			static bool init = false;
			static Iterator iter;

			if (init) { // create container from iterators
				init = false;
				captures.emplace_back(iter, iterator);
			} else { // save iterator
				iter = iterator;
				init = true;
			}
		}
	};

	// matchers
	template <Type... T>
	struct Literal : internal::Rule<
							  Literal<T...>,
							  internal::Flags::Data> {
		static_assert(sizeof...(T) > 0, "Must provide at least one literal");

		static constexpr std::initializer_list<Type> data = { T... };
	};

	template <unsigned int Amount = 1>
	struct Any : internal::Rule<
						 Any<Amount>,
						 internal::Flags::Matcher
							 | internal::Flags::MatcherCheckIteratorBounds> {
		static_assert(Amount > 0, "Amount must be at least one");

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D&) {
			if (iterator + Amount <= end) {
				iterator += Amount;
				return true;
			}

			return false;
		}
	};

	template <Type... T>
	struct AnyLiteral : internal::Rule<
								  AnyLiteral<T...>,
								  internal::Flags::Data
									  | internal::Flags::DataAnyOf
									  | internal::Flags::MatcherCheckIteratorBounds> {
		static_assert(sizeof...(T) > 0, "Must provide at least one literal");

		static constexpr std::initializer_list<Type> data = { T... };
	};

	template <class... T>
	struct AnyRule : internal::Rule<
							  AnyRule<T...>,
							  internal::Flags::Matcher
								  | internal::Flags::MatcherCheckIteratorBounds
								  | internal::Flags::MatcherAdvanceIteratorOnMatch> {
		static_assert(sizeof...(T) > 0, "Must provide at least one rule");
		static_assert((... && internal::advances_iterator<T>()), "Only matchers are allowed as children");

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			return (... || internal::process<T>(iterator, end, data));
		}
	};

	template <class T>
	struct Optional : internal::Rule<
								Optional<T>,
								internal::Flags::Matcher
									| internal::Flags::MatcherCheckIteratorBounds> {
		static_assert(internal::advances_iterator<T>(), "Only matchers are allowed as children");

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			auto iter = iterator;
			if (internal::process<T>(iter, end, data))
				iterator = iter;

			return true;
		}
	};

	template <class... T>
	struct Sequence : internal::Rule<
								Sequence<T...>,
								internal::Flags::Matcher
									| internal::Flags::MatcherCheckIteratorBounds
									| internal::Flags::MatcherAdvanceIteratorOnMatch> {
		static_assert(sizeof...(T) > 0, "Must have at least one child");

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			return (... && internal::process<T>(iterator, end, data));
		}
	};

	template <class T, int Amount = 0>
	struct Repeat : internal::Rule<
							 Repeat<T, Amount>,
							 internal::Flags::Matcher
								 | internal::Flags::MatcherCheckIteratorBounds> {
		static_assert(Amount >= 0, "Amount must be equal or more than zero");
		static_assert(internal::advances_iterator<T>(), "Child must be a matcher");

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			auto iter = iterator;

			if constexpr (Amount == 0) {
				while (internal::process<T>(iter, end, data))
					;

				if (iter <= end) {
					iterator = iter;
					return true;
				}

			} else {
				int matches = 0;
				for (; matches < Amount; ++matches)
					if (!internal::process<T>(iter, end, data))
						return false;

				if (matches == Amount && iter <= end) {
					iterator = iter;
					return true;
				}

				return false;
			}
		}
	};

	template <class T, int Minimum, int Maximum>
	struct MatchBetween : internal::Rule<
									 MatchBetween<T, Minimum, Maximum>,
									 internal::Flags::Matcher
										 | internal::Flags::MatcherCheckIteratorBounds
										 | internal::Flags::MatcherAdvanceIteratorOnMatch> {
		static_assert(Minimum >= 0, "Minimum must be equal or more than zero");
		static_assert(Minimum < Maximum, "Minimum must be less than Maximum by at least one");

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			int matches = 0;
			for (; matches < Maximum; ++matches)
				if (!internal::process<T>(iterator, end, data))
					break;

			return matches >= Minimum;
		}
	};

	// conditionals
	template <class C, class T>
	struct If_then : internal::Rule<
							  If_then<C, T>,
							  internal::Flags::Matcher
								  | internal::MatcherAdvanceIteratorOnMatch> {

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			if (auto iter = iterator; internal::process<C>(iter, end, data))
				return internal::process<T>(iterator, end, data);

			return false;
		}
	};

	template <class C, class T, class F>
	struct If_else : internal::Rule<
							  If_else<C, T, F>,
							  internal::Flags::Matcher
								  | internal::Flags::MatcherAdvanceIteratorOnMatch> {

		template <typename D>
		static bool process(Iterator& iterator, ConstIterator& end, D& data) {
			if (auto iter = iterator; internal::process<C>(iter, end, data))
				return internal::process<T>(iterator, end, data);
			else
				return internal::process<F>(iterator, end, data);
		}
	};

	template <class T>
	struct Not : internal::Rule<
						 Not<T>,
						 internal::Flags::Matcher> {

		template <typename D>
		static bool process(Iterator iterator, ConstIterator& end, D& data) {
			return internal::process<T>(iterator, end, data);
		}
	};

	//template <typename T = ContainerType>
	//struct ContainerSpecific {};

	template <typename T = Type>
	struct TypeSpecific {};

#define QUICK_RULE(name, fun)                                              \
	struct name : internal::Rule<                                           \
						  name,                                                  \
						  internal::Flags::Matcher                               \
							  | internal::Flags::MatcherAdvanceIteratorOnMatch> { \
		template <typename D>                                                \
		static bool process(Iterator iterator, ConstIterator& end, D&) {     \
			return fun(*iterator++);                                          \
		}                                                                    \
	};

	template <>
	struct TypeSpecific<char> {
		QUICK_RULE(Ctrnl, iscntrl)
		QUICK_RULE(Blank, isblank)
		QUICK_RULE(Space, isspace)
		QUICK_RULE(Upper, isupper)
		QUICK_RULE(Lower, islower)
		QUICK_RULE(Alpha, isalpha)
		QUICK_RULE(Digit, isdigit)
		QUICK_RULE(Xdigit, isxdigit)
		QUICK_RULE(Alnum, isalnum)
		QUICK_RULE(Punct, ispunct)
		QUICK_RULE(Graph, isgraph)
		QUICK_RULE(Print, isprint)
	};

#undef QUICK_RULE
}

#undef libparse_TYPE
