# Copyright 2018-2019 Aleksandar Radivojevic
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#	 http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

cmake_minimum_required (VERSION 3.6 FATAL_ERROR)

# disable in-source build
if (CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR)
	message (FATAL_ERROR "In-source build is not permitted")
endif ()

# set build to debug by default
if (NOT CMAKE_BUILD_TYPE)
	set (CMAKE_BUILD_TYPE Release CACHE STRING "Choose the type of build (Debug, Release)" FORCE)
endif ()

project (libparse LANGUAGES CXX)

# read version from file
file (READ "include/libparse/libparse.hh" VERSION_FILE)
string (REGEX MATCH "#define libparse_VERSION_MAJOR +([0-9]+)" _ ${VERSION_FILE})
if (NOT CMAKE_MATCH_COUNT GREATER 0)
	message (FATAL_ERROR "Could not read major version from file")
endif ()
set (libparse_VERSION_MAJOR ${CMAKE_MATCH_1})

string (REGEX MATCH "#define libparse_VERSION_MINOR +([0-9]+)" _ ${VERSION_FILE})
if (NOT CMAKE_MATCH_COUNT GREATER 0)
	message (FATAL_ERROR "Could not read minor version from file")
endif()
set (libparse_VERSION_MINOR ${CMAKE_MATCH_1})

string (REGEX MATCH "#define libparse_VERSION_PATCH +([0-9]+)" _ ${VERSION_FILE})
if (NOT CMAKE_MATCH_COUNT GREATER 0)
	message (FATAL_ERROR "Could not read patch version from file")
endif ()
set (libparse_VERSION_PATCH ${CMAKE_MATCH_1})
unset (VERSION_FILE)

# detect if subproject
if (NOT "libparse" STREQUAL "${CMAKE_PROJECT_NAME}")
	set (libparse_SUBPROJECT ON CACHE BOOL "Disables tests, examples, benchmarks automatically")
endif ()

# options
option (libparse_BUILD_TESTS "Build tests" ON)
option (libparse_BUILD_EXAMPLES "Build examples" ON)
option (libparse_BUILD_BENCHMARK "Build benchmark" ON)

if (NOT libparse_SUBPROJECT)
	message (STATUS "libparse version ${libparse_VERSION_MAJOR}.${libparse_VERSION_MINOR}.${libparse_VERSION_PATCH}")
endif ()

if (NOT libparse_THIRD_PARTY)
	set (libparse_THIRD_PARTY ${CMAKE_CURRENT_SOURCE_DIR}/third_party CACHE PATH "Path to libraries")
endif ()

# add the library
add_library (libparse INTERFACE)
add_library (libparse::libparse ALIAS libparse)
target_compile_features (libparse INTERFACE cxx_std_17)

# include
target_include_directories (libparse
	INTERFACE
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
		$<INSTALL_INTERFACE:include>
)

# supress messages from subprojects unless it's an error or warning
if (NOT libparse_SUBPROJECT AND (libparse_BUILD_EXAMPLES OR libparse_BUILD_BENCHMARK OR libparse_BUILD_TESTS))
	function (message)
		if (NOT SUPRESS_MESSAGE)
			_message(${ARGN})
		else ()
			list (GET ARGN 0 mode)
			if (NOT "${mode}" STREQUAL "STATUS")
				_message(${ARGN})
			endif ()
		endif ()
	endfunction ()
endif ()

# add subdirectory but set build type to release
macro (add_release_subdirectory)
	set (OLD_BUILD_TYPE ${CMAKE_BUILD_TYPE})
	set (CMAKE_BUILD_TYPE RelWithDebInfo)

	add_subdirectory(${ARGV})

	set (CMAKE_BUILD_TYPE ${OLD_BUILD_TYPE})
	unset (OLD_BUILD_TYPE)
endmacro ()

# examples
if (libparse_BUILD_EXAMPLES AND NOT libparse_SUBPROJECT)
	message(STATUS "Configuring examples")
	add_subdirectory (examples)
endif ()

# benchmarks
if (libparse_BUILD_BENCHMARK AND NOT libparse_SUBPROJECT)
	message(STATUS "Configuring benchmarks")
	add_subdirectory (benchmark)
endif ()

# testing
if (libparse_BUILD_TESTS AND NOT libparse_SUBPROJECT)
	message(STATUS "Configuring tests")
	enable_testing ()
	add_subdirectory (tests)
endif ()
