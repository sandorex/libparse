// Copyright 2018-2019 Aleksandar Radivojevic
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// You may obtain a copy of the License at
// 	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <deque>
#include <gtest/gtest.h>
#include <iostream>
#include <string>

#include <libparse/libparse.hh>
using namespace libparse;
using namespace libparse::internal;

const std::string input_str = "abcd";
const std::string_view input = input_str;
const auto cbegin = input.cbegin();
const auto cend = input.cend();

TEST(parser, capture) {
	auto iter = cbegin;
	auto iterEnd = cend;

	using Capturer1 = Capture<Literal<'a', 'b', 'c', 'd'>>;

	ASSERT_TRUE((Capturer1::match(iter, cend)));
	ASSERT_EQ(iter, cend);

	ASSERT_EQ(Capturer1::captures, std::deque<std::string> { input_str });

	iter = cbegin;

	ASSERT_TRUE((Sequence<Capture<>, Literal<'a', 'b', 'c', 'd'>, Capture<>>::match(iter, cend)));
	ASSERT_EQ(iter, cend);

	ASSERT_EQ(Capture<>::captures, std::deque<std::string> { input_str });
}

TEST(parser, literal) {
	auto iter = cbegin;
	auto iterEnd = cend;

	using FULL = Literal<'a', 'b', 'c', 'd'>;
	using OVERFLOW = Literal<'a', 'b', 'c', 'd', 'e'>;

	// check bounds
	ASSERT_FALSE((OVERFLOW::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	ASSERT_FALSE((OVERFLOW::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((FULL::match(iter, cend)));
	ASSERT_EQ(iter, cend);

	iter = cbegin;

	// single char literal uses different code
	ASSERT_TRUE((Literal<'a'>::match(iter, cend)));
	ASSERT_TRUE((Literal<'b'>::match(iter, cend)));
	ASSERT_TRUE((Literal<'c'>::match(iter, cend)));
	ASSERT_TRUE((Literal<'d'>::match(iter, cend)));
	ASSERT_FALSE((Literal<'e'>::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}

TEST(parser, any) {
	auto iter = cbegin;
	auto iterEnd = cend;

	// check bounds
	ASSERT_FALSE((Any<5>::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	ASSERT_FALSE((Any<>::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((Any<4>::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}

TEST(parser, anyLiteral) {
	auto iter = cbegin;
	auto iterEnd = cend;

	// check bounds
	ASSERT_FALSE((AnyLiteral<'b', 'a'>::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((AnyLiteral<'d', 'c', 'b', 'a'>::match(iter, cend)));
	ASSERT_TRUE((AnyLiteral<'d', 'c', 'b', 'a'>::match(iter, cend)));
	ASSERT_TRUE((AnyLiteral<'d', 'c', 'b', 'a'>::match(iter, cend)));
	ASSERT_TRUE((AnyLiteral<'d', 'c', 'b', 'a'>::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}

TEST(parser, anyRule) {
	auto iter = cbegin;
	auto iterEnd = cend;

	using Alias = AnyRule<Literal<'d'>, Literal<'c'>, Literal<'b'>, Literal<'a'>>;

	// check bounds
	ASSERT_FALSE((Alias::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	EXPECT_TRUE((Alias::match(iter, cend)));
	EXPECT_TRUE((Alias::match(iter, cend)));
	EXPECT_TRUE((Alias::match(iter, cend)));
	EXPECT_TRUE((Alias::match(iter, cend)));
	EXPECT_EQ(iter, cend);
}

TEST(parser, optional) {
	auto iter = cbegin;
	auto iterEnd = cend;

	// check bounds
	ASSERT_TRUE((Optional<Literal<'a', 'b', 'c', 'd', 'e'>>::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	// false only if iter >= cend
	ASSERT_FALSE((Optional<Literal<'a'>>::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((Optional<Literal<'?'>>::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	ASSERT_TRUE((Optional<Literal<'a', 'b', 'c', 'd'>>::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}

TEST(parser, sequence) {
	auto iter = cbegin;
	auto iterEnd = cend;

	using Alias = Sequence<Literal<'a'>, Literal<'b'>, Literal<'c'>, Literal<'d'>>;

	// check bounds
	ASSERT_FALSE((Sequence<Literal<'a'>, Literal<'b'>, Literal<'c'>, Literal<'d'>, Literal<'e'>>::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	ASSERT_FALSE((Alias::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((Alias::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}

TEST(parser, repeat) {
	auto iter = cbegin;
	auto iterEnd = cend;

	using Alias = AnyLiteral<'a', 'b', 'c', 'd'>;

	// check bounds
	ASSERT_FALSE((Repeat<Alias, 5>::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	ASSERT_FALSE((Repeat<Alias, 5>::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((Repeat<Alias, 4>::match(iter, cend)));
	ASSERT_EQ(iter, cend);

	iter = cbegin;

	// match as many as possible
	ASSERT_TRUE((Repeat<Alias>::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}

TEST(parser, matchBetween) {
	auto iter = cbegin;
	auto iterEnd = cend;

	using Alias = AnyLiteral<'a', 'b', 'c', 'd'>;

	// check bounds
	ASSERT_FALSE((MatchBetween<Alias, 5, 6>::match(iter, cend)));
	ASSERT_EQ(iter, cbegin);

	ASSERT_FALSE((MatchBetween<Alias, 4, 5>::match(iterEnd, cend)));
	ASSERT_EQ(iterEnd, cend);

	// check match
	ASSERT_TRUE((MatchBetween<Alias, 4, 5>::match(iter, cend)));
	ASSERT_EQ(iter, cend);
}
