// Copyright 2018 Aleksandar Radivojevic
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// You may obtain a copy of the License at
// 	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <benchmark/benchmark.h>
#include <libparse/lexer.hh>
#include <string>

#define ITERATIONS 1000

static void first(benchmark::State& state) {
	const std::string_view input = "hello";

	for (auto _ : state)
		libparse::lexer::tokenize_first(input);
}

static void second(benchmark::State& state) {
	const std::string_view input = "hello";

	for (auto _ : state)
		libparse::lexer::tokenize_second(input);
}

//BENCHMARK(first)->Iterations(ITERATIONS);
BENCHMARK(second)->Iterations(ITERATIONS);
BENCHMARK_MAIN();
