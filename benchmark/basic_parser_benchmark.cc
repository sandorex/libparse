// Copyright 2018 Aleksandar Radivojevic
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
//
// You may obtain a copy of the License at
// 	 http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <array>
#include <benchmark/benchmark.h>
#include <string>
#include <utility>
#include <vector>

#include <libparse/libparse.hh>
using namespace libparse;
using namespace libparse::internal;

#define ITERATIONS 100000

const std::string_view input = "a";
const auto cbegin = input.cbegin();
const auto cend = input.cend();

static void parser_literal(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<Literal<'a'>>(iter, cend);
}

static void parser_any(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<Any<>>(iter, cend);
}

static void parser_anyliteral(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<AnyLiteral<'a'>>(iter, cend);
}

static void parser_anyrule_literal(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<AnyRule<Literal<'a'>>>(iter, cend);
}

static void parser_optional_literal(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<Optional<Literal<'a'>>>(iter, cend);
}

static void parser_sequence_literal(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<Sequence<Literal<'a'>>>(iter, cend);
}

static void parser_repeat_literal(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<Repeat<Literal<'a'>, 1>>(iter, cend);
}

static void parser_matchbetween_literal(benchmark::State& state) {
	auto iter = cbegin;

	for (auto _ : state)
		process<MatchBetween<Literal<'a'>, 0, 1>>(iter, cend);
}

BENCHMARK(parser_literal)->Iterations(ITERATIONS);
BENCHMARK(parser_any)->Iterations(ITERATIONS);
BENCHMARK(parser_anyliteral)->Iterations(ITERATIONS);
BENCHMARK(parser_anyrule_literal)->Iterations(ITERATIONS);
BENCHMARK(parser_optional_literal)->Iterations(ITERATIONS);
BENCHMARK(parser_sequence_literal)->Iterations(ITERATIONS);
BENCHMARK(parser_repeat_literal)->Iterations(ITERATIONS);
BENCHMARK(parser_matchbetween_literal)->Iterations(ITERATIONS);
